# voltdb-simple-stats

This is used for pulling stats out of VoltDB on an ad hoc basis.

## Usage ##

java -jar voltdb-simple-stats.jar hostnames PARSABLE [PARSABLE_RAM_USAGE | PARSABLE_CLIENT_COUNT ]


or

java -jar voltdb-simple-stats.jar  hostnames VOLTSTAT voltstatname 