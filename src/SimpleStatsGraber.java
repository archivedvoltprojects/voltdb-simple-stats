import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.voltdb.VoltTable;
import org.voltdb.client.Client;
import org.voltdb.client.ClientConfig;
import org.voltdb.client.ClientFactory;
import org.voltdb.client.ClientResponse;
import org.voltdb.client.NoConnectionsException;
import org.voltdb.client.ProcCallException;

/* This file is part of VoltDB.
 * Copyright (C) 2008-2019 VoltDB Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

public class SimpleStatsGraber {

    private static final String PARSABLE_RAM_USAGE = "PARSABLE_RAM_USAGE";
    private static final String PARSABLE_CLIENT_COUNT = "PARSABLE_CLIENT_COUNT";
    private static final String PARSABLE = "PARSABLE";
    private static final String VOLTSTAT = "VOLTSTAT";

    public static void main(String[] args) {

        Client client = null;

        try {

            if (args.length != 3) {
                printUsageAndExit();
            }

            final String hostnames = args[0];
            final String parsable = args[1];
            final String mode = args[2];

            if (!(parsable.equalsIgnoreCase(PARSABLE) || parsable.equalsIgnoreCase(VOLTSTAT))) {
                printUsageAndExit();
            }

            client = connectVoltDB(hostnames);

            if (parsable.equalsIgnoreCase(PARSABLE)) {
                
                if (mode.equalsIgnoreCase(PARSABLE_RAM_USAGE)) {
                    
                    getParsableRamUsage(client);

                } else if (mode.equalsIgnoreCase(PARSABLE_CLIENT_COUNT)) {
                    
                    getParsableClientCount(client);

                }
            } else { // VOLTSTAT
                
                ClientResponse cr = client.callProcedure("@Statistics", mode, 0);
                VoltTable[] results = cr.getResults();
                
                for (int t=0; t < results.length; t++) {
                    System.out.println(results[t].toFormattedString());
                }

            }
            
            client.close();
            System.exit(0);

        } catch (Exception e) {

            if (client != null) {
                try {
                    client.close();
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }

            msg(e.getClass().getName() + ":" + e.getMessage());
            System.exit(1);
        }

    }

    /**
     * @param client
     * @throws IOException
     * @throws NoConnectionsException
     * @throws ProcCallException
     */
    private static void getParsableClientCount(Client client)
            throws IOException, NoConnectionsException, ProcCallException {
        ClientResponse cr = client.callProcedure("@Statistics", "LIVECLIENTS", 0);

        VoltTable clientTable = cr.getResults()[0];
        System.out.println(clientTable.getRowCount());
    }

    /**
     * @param client
     * @throws IOException
     * @throws NoConnectionsException
     * @throws ProcCallException
     */
    private static void getParsableRamUsage(Client client)
            throws IOException, NoConnectionsException, ProcCallException {
        ClientResponse cr = client.callProcedure("@Statistics", "MEMORY", 0);

        VoltTable ramTable = cr.getResults()[0];

        int ramPct = Integer.MIN_VALUE;

        while (ramTable.advanceRow()) {

            long ramUsed = ramTable.getLong("RSS");
            long totalRam = ramTable.getLong("PHYSICALMEMORY");
            int ramcPctThisNode = (int) ((100 * ramUsed) / totalRam);

            if (ramcPctThisNode > ramPct) {
                ramPct = ramcPctThisNode;
            }
        }

        System.out.println(ramPct);
    }

    private static void printUsageAndExit() {

        msg("Usage: java -jar voltdb-simple-stats.jar hostnames " + PARSABLE + " [" + PARSABLE_RAM_USAGE + " | " + PARSABLE_CLIENT_COUNT
                + " ]");
        msg("or");
        msg("Usage: java -jar voltdb-simple-stats.jar  hostnames " + VOLTSTAT + " voltstatname ");
        System.exit(2);

    }

    private static Client connectVoltDB(String hostnames) throws Exception {
        Client client = null;
        ClientConfig config = null;

        try {
            config = new ClientConfig(); // "admin", "idontknow");

            client = ClientFactory.createClient(config);
            String[] hostnameArray = hostnames.split(",");

            for (int i = 0; i < hostnameArray.length; i++) {
                try {
                    client.createConnection(hostnameArray[i]);
                } catch (Exception e) {
                    msg(e.getMessage());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("VoltDB connection failed.." + e.getMessage(), e);
        }

        return client;

    }

    public static void msg(String message) {

        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        System.out.println(strDate + ":" + message);
    }

}
